export const apiPath = {
  venom: 'customer_service',
  eider: 'message_broker',
  eagle: 'chatroom_betting',
  donald: 'interest_bao',
  fox: 'general'
}

export const localStorageKey = {
  ghost: 'ghost_token',
  eider: 'eider_setting',
  eagle: 'eagle_setting',
  donald: 'donald_setting',
  fox: 'fox_setting'
}
