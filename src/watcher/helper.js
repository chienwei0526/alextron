import ghost from '@/api/ghost'
import { getBearerToken } from '@/utils'
import Vue from 'vue'
import store from '@/store'

class GhostHeartBeat {
  constructor ({ interval = 5 * 60 * 1000 } = {}) {
    this.interval = interval
    this.timer = null
  }
  start () {
    ghost.sendHeartBeat().catch(() => {})
    this.clear()
    this.timer = setInterval(() => {
      ghost.sendHeartBeat().catch(() => {})
    }, this.interval)
  }
  clear () {
    clearInterval(this.timer)
  }
}

export { GhostHeartBeat }

class GhostTokenChecker {
  constructor ({ interval = 1000 } = {}) {
    this.interval = interval
    this.timer = null
  }
  start () {
    this.clear()
    this.timer = setTimeout(async () => {
      const tokenExpire = getBearerToken('ghost_token', 'expires_in')
      if (tokenExpire) {
        const refresh = getBearerToken('ghost_token', 'refresh_token')
        if (Vue.moment().isAfter(tokenExpire)) {
          const data = await ghost.getToken(refresh)
          store.commit('token/add', {
            type: 'ghost',
            setting: data
          })
        }
      }
      this.start()
    }, this.interval)
  }
  clear () {
    clearTimeout(this.timer)
  }
}

export { GhostTokenChecker }

export function dispatch (...actions) {
  return Promise.all(
    actions.map(action => store.dispatch(action))
  )
}
