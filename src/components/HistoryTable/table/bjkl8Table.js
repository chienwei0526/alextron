export const bjkl8Table =
[
  {
    displayName: '时间',
    key: 'schedule_result'
  },
  {
    displayName: '期数',
    key: 'issue_number'
  },
  {
    displayName: '开奖号码',
    key: 'result_str'
  },
  {
    displayName: '开奖',
    subHeads: [
      {
        displayName: '号码',
        key: 'sum_of_ball'
      },
      {
        displayName: '大小',
        key: 'sum_of_ball_than_size'
      },
      {
        displayName: '单双',
        key: 'sum_of_ball_odd_even'
      },
      {
        displayName: '五行',
        key: 'sum_of_ball_five_element'
      }
    ]
  },
  {
    displayName: '比数量',
    subHeads: [
      {
        displayName: '单双比',
        key: 'balls_odd_even_cp'
      },
      {
        displayName: '前后比',
        key: 'balls_front_rear_count_cp'
      }
    ]
  }
]
