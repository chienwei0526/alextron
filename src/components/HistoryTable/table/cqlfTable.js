export const cqlfTable =
[
  {
    displayName: '时间',
    key: 'schedule_result'
  },
  {
    displayName: '期数',
    key: 'issue_number'
  },
  {
    displayName: '',
    buttons: [
      {
        displayName: '号码',
        show: 'number'
      },
      {
        displayName: '大小',
        show: 'thanSize'
      },
      {
        displayName: '单双',
        show: 'oddEven'
      },
      {
        displayName: '尾大小',
        show: 'tailThanSize'
      },
      {
        displayName: '合单双',
        show: 'ballOfSumOddEven'
      }
    ]
  },
  {
    displayName: '总和',
    subHeads: [
      {
        displayName: '号码',
        key: 'sum_of_ball'
      },
      {
        displayName: '大小',
        key: 'sum_of_ball_than_size'
      },
      {
        displayName: '单双',
        key: 'sum_of_ball_odd_even'
      },
      {
        displayName: '尾大小',
        key: 'sum_of_ball_tail_than_size'
      }
    ]
  },
  {
    displayName: '龙虎',
    subHeads: [
      {
        displayName: '',
        key: 'dragon_tiger_1_8'
      },
      {
        displayName: '',
        key: 'dragon_tiger_2_7'
      },
      {
        displayName: '',
        key: 'dragon_tiger_3_6'
      },
      {
        displayName: '',
        key: 'dragon_tiger_4_5'
      }
    ]
  }
]
