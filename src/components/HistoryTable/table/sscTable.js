export const sscTable =
[
  {
    displayName: '时间',
    key: 'schedule_result'
  },
  {
    displayName: '期数',
    key: 'issue_number'
  },
  {
    displayName: '',
    buttons: [
      {
        displayName: '显示号码',
        show: 'number'
      },
      {
        displayName: '显示大小',
        show: 'thanSize'
      },
      {
        displayName: '显示单双',
        show: 'oddEven'
      }
    ]
  },
  {
    displayName: '总和',
    subHeads: [
      {
        displayName: '号码',
        key: 'sum_of_ball'
      },
      {
        displayName: '大小',
        key: 'sum_of_ball_than_size'
      },
      {
        displayName: '单双',
        key: 'sum_of_ball_odd_even'
      }
    ]
  },
  {
    displayName: '龙虎',
    key: 'dragon_tiger_1_5'
  },
  {
    displayName: '前三',
    key: 'weird_chinese_dice_rules_1_3'
  },
  {
    displayName: '中三',
    key: 'weird_chinese_dice_rules_2_4'
  },
  {
    displayName: '后三',
    key: 'weird_chinese_dice_rules_3_5'
  }
]
