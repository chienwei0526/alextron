import { transformerTable } from './transformerTable'
import { sscTable } from './sscTable'
import { cqlfTable } from './cqlfTable'
import { jsk3Table } from './jsk3Table'
import { pcddTable } from './pcddTable'
import { gd11x5Table } from './gd11x5Table'
import { bjkl8Table } from './bjkl8Table'
import { hklTable } from './hklTable'
import { auluck8Table } from './auluck8Table'
import { msnnTable } from './msnnTable'
import { fc3dTable } from './fc3dTable'

const transformerTableGames = ['jspk10', 'mspk10', 'bcr', 'mlaft', 'fift', 'er75ft', 'cs60cr', 'cs600cr', 'fifcr']
const sscTableGames = ['cstw5fc', 'twwfc', 'cqssc', 'jsssc', 'xjssc', 'tjssc', 'csffc', 'cs10fc', 'cs5fc', 'ynssc', 'hjssc', 'fifc', 'csggc']
const cqlfTableGames = ['cqlf', 'gdklsf']
const jsk3TableGames = ['jsk3', 'msk3', 'bjk3', 'gxk3', 'shk3', 'hubk3', 'gzk3', 'hbk3', 'gsk3', 'glk3', 'luckyk3', 'fik3', 'ahk3', 'fjk3', 'nmk3']
const pcddTableGames = ['pcdd', 'jnd28', 'luckdd', 'msdd']
const gd11x5TableGames = ['gd11x5', 'ah11x5', 'bj11x5', 'fj11x5', 'gs11x5', 'ms11x5', 'gx11x5']
const bjkl8TableGames = ['bjkl8']
const hklTableGames = ['hkl', 'luckl', 'cshkl', 'cs600hkl', 'csjndhkl', 'cs75hkl', 'fihkl']
const auluck8TableGames = ['auluck8']
const msnnTableGames = ['msnn', 'pk10nn']
const fc3dTableGames = ['fc3d']

const mapList = [
  {
    games: transformerTableGames,
    table: transformerTable
  },
  {
    games: sscTableGames,
    table: sscTable
  },
  {
    games: cqlfTableGames,
    table: cqlfTable
  },
  {
    games: jsk3TableGames,
    table: jsk3Table
  },
  {
    games: pcddTableGames,
    table: pcddTable
  },
  {
    games: gd11x5TableGames,
    table: gd11x5Table
  },
  {
    games: bjkl8TableGames,
    table: bjkl8Table
  },
  {
    games: hklTableGames,
    table: hklTable
  },
  {
    games: auluck8TableGames,
    table: auluck8Table
  },
  {
    games: fc3dTableGames,
    table: fc3dTable
  },
  {
    games: msnnTableGames,
    table: msnnTable
  }
]

const tableMap = mapList.reduce((acc, { games, table }) => {
  games.forEach(code => {
    acc[code] = table
  })
  return acc
}, {})

export const getTable = gameCode => {
  return tableMap[gameCode] || []
}
