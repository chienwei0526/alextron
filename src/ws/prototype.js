import { throwError } from 'rxjs'
import { retryWhen, delay, tap, concatMap } from 'rxjs/operators'
import { webSocket } from 'rxjs/webSocket'

class ObservableWebsocket {
  constructor (url) {
    this.onMessage = () => {}

    this.whenWebsocketHasError = () => {}

    this._connectOp = () => {
      this.ws = webSocket(url)
      this.ws.pipe(
        retryWhen(errorHandler$)
      ).subscribe(
        res => this.onMessage(res), // 成功
        () => this._connectOp() // 錯誤
      )
    }

    const errorHandler$ = errors => errors.pipe(
      tap(this.whenWebsocketHasError),
      delay(5000),
      concatMap(throwError)
    )
  }

  connect () {
    this._connectOp()
  }

  close () {
    this.ws.complete()
    this.ws = null
  }
}

export { ObservableWebsocket }
