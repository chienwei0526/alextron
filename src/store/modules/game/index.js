import ghost from '@/api/ghost'
import { saveLastGameData, getLastGameData } from '@/utils'

export default {
  namespaced: true,
  state: {
    games: [],
    categories: [],
    playGroups: {},
    classification: [],
    lastGameData: getLastGameData()
  },
  getters: {
    currentGame: (state, getters, rootState, rootGetters) => {
      const gameId = rootState.route.params.gameId
      if (!gameId || state.games.length === 0) {
        return null
      }
      let game = state.games.find(g => g.id + '' === gameId)
      if (!game) {
        return null
      }

      let gameType = game.type.code
      if (gameType === 'other') gameType = game.code
      return {
        ...game,
        type: gameType
      }
    }
  },
  mutations: {
    init: (state, { games }) => {
      state.games = games
    },
    setCategories: (state, categories) => {
      state.categories = { ...state.categories, ...categories }
    },
    setPlayGroup: (state, playGroups) => {
      state.playGroups = playGroups
    },
    cachePlayGroup: (state, playGroups) => {
      state.playGroups = { ...state.playGroups, ...playGroups }
    },
    setClassification: (state, data) => {
      state.classification = data
    },
    saveLastGame: (state, id) => {
      state.lastGameData.lastGame = id
      saveLastGameData(state.lastGameData)
    }
  },
  actions: {
    fetch: ({ commit, state }) => {
      commit('setPlayGroup', {}) // 清除緩存
      return ghost.fetchGames().then(res => {
        commit('init', {
          games: res
        })

        ghost.fetchGamesDetail().then(games => {
          const totalCategories = {}
          const classificationMap = {}
          const classificationList = []

          games.forEach((game) => {
            let groupTag = game.group_tag
            let groupTagKey = groupTag.code

            let gameList
            if (classificationMap[groupTagKey]) {
              gameList = classificationMap[groupTagKey]
            } else {
              gameList = []
              classificationMap[groupTagKey] = gameList
              classificationList.push({ tag: groupTagKey, games: gameList, name: groupTag.name, hasAccumulationStatistic: groupTag.has_accumulation_statistic })
            }

            gameList.push({
              code: game.code,
              display_name: game.display_name,
              rank: game.rank,
              icon: game.icon,
              label: game.label,
              id: game.id
            })

            const categories = game.categories
            delete categories.playgroups
            totalCategories[game.id] = categories
          })

          classificationList.forEach(item => {
            item.games.sort((a, b) => a.rank - b.rank)
          })
          commit('setClassification', classificationList)
          commit('setCategories', totalCategories)
        })
      })
    },
    fetchPlayGroups: ({ commit }, categoryId) => {
      return ghost.fetchPlayGroups(categoryId).then(res => {
        commit('cachePlayGroup', { [categoryId]: res })
        return res
      })
    }
  }
}
