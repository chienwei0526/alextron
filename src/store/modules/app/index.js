import ghost from '@/api/ghost'

const state = {
  navbar: {
    active: 'home'
  },
  systemConfig: {
    state: 'pending',
    envelopeActivityId: null,
    serviceAction: null,
    slogan: '',
    subhead: '',
    agentDashboardUrl: '',
    homePageLogo: '',
    siteName: '',
    global_preferences: {
      chat_condition_message: ''
    },
    floatAd: null,
    contactPhoneNumber: '',
    openAccountConsultingQQ: '',
    agentBusinessConsultingQQ: '',
    contactEmail: '',
    mobileLinkUrl: '',
    smsValidationEnabled: 'false',
    planSiteSetting: {
      host: '',
      games: []
    },
    donateEnabled: false
  }

}

const mutations = {
  changeActiveLink: (state, linkName) => {
    state.navbar.active = linkName
  },
  setSystemConfig: (state, config) => {
    state.systemConfig = config
  }
}

const actions = {
  fetchSystemConfig ({ commit, dispatch }) {
    ghost.getHomePage().then(
      response => {
        let pref = response.global_preferences || {}
        const customerServiceUrl = pref.customer_service_url
        const enableBuiltInCustomerService = pref.enable_built_in_customer_service === 'true'
        let serviceAction = null
        if (enableBuiltInCustomerService) {
          serviceAction = () => {
            dispatch('customerService/open')
          }
        } else {
          if (customerServiceUrl) {
            serviceAction = () => { window.open(customerServiceUrl) }
          }
        }

        commit('setSystemConfig',
          {
            state: 'fulfilled',
            serviceAction,
            homePageLogo: response.icon,
            slogan: response.slogan,
            subhead: response.subhead,
            agent_type: response.agent_type,
            agentDashboardUrl: pref.agent_dashboard_url,
            global_preferences: pref,
            agentBusinessConsultingQQ: response.agent_business_consulting_qq,
            contactEmail: response.contact_email,
            contactPhoneNumber: response.contact_phone_number,
            customFields: response.custom_fields,
            openAccountConsultingQQ: response.open_account_consulting_qq,
            siteName: response.name,
            floatAd: response.right_float_img,
            gaTrackingId: pref.ga_tracking_id,
            favicon: response.favicon,
            regPresentAmount: response.reg_present_amount,
            needBankinfo: response.need_bankinfo,
            chatroomEnvelopeSettings: pref.chatroom_red_envelope_eagle || {},
            mobileLinkUrl: pref.mobile_link_url,
            smsValidationEnabled: pref.sms_validation_enabled,
            planSiteSetting: {
              host: pref.plan_site_url || '',
              games: ['bcr', 'cqssc', 'jsssc', 'jspk10', 'mspk10', 'mlaft', 'fift', 'fifc']
            },
            envelopeActivityId: response.envelope_activity_id || null,
            theme: response.theme || 'default',
            appDownloadUrl: pref.app_download_url,
            enableBuiltInCustomerService: pref.enable_built_in_customer_service === 'true',
            domains: response.domains,
            donateEnabled: pref.donate_enabled === true
          })
      }
    )
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
