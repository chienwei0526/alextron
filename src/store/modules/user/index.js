import ghost from '@/api/ghost'
import venom from '@/api/venom'
import token from '../token'
import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    logined: 'pending',
    account_type: undefined,
    unsettled: 0,
    onlinePaymentTypes: null,
    totalBalance: 0,
    agent: null
  },
  mutations: {
    init: (state, user) => {
      Object.assign(state, user)
    },
    reset: (state) => {
      Object.assign(state, {
        logined: false,
        account_type: undefined,
        unsettled: 0,
        onlinePaymentTypes: null,
        totalBalance: 0,
        agent: null
      })
    }
  },
  actions: {
    login: ({ commit, state, dispatch }, { user }) => {
      return ghost.login(user).then(res => {
        if (res.access_token && res.refresh_token) {
          commit('token/add', {
            type: 'ghost',
            setting: res
          }, { root: true })
          commit('init', { ...user })
        }
        return dispatch('fetch')
      }, error => {
        if (error.code === 9011 || error.code === 9013) {
          Vue.cookie.set('sessionid', error.data.sessionid)
        }
        return Promise.reject(error)
      })
    },
    trial: ({ commit, state, dispatch }, data) => {
      if (state.loading) {
        return
      }
      let payload = {
        ...((data && data.verification_code_0 && data.verification_code_1) && {
          verification_code_0: data.verification_code_0,
          verification_code_1: data.verification_code_1
        })
      }

      return ghost.trial(payload).then((response) => {
        if (response.trial_auth_req === 1) {
          return Promise.reject(response)
        }
        let tokenObj = response.token
        if (tokenObj.access_token && tokenObj.refresh_token) {
          commit('token/add', {
            type: 'ghost',
            token: tokenObj
          }, {
            root: true
          })

          if (token.venom) { venom.serviceTrial().catch(() => {}) }
          if (response.agent) {
            commit('init', {
              ...state.user,
              agent: response.agent
            })
          }
        }

        return dispatch('fetch')
      }).catch((errRes) => {
        return Promise.reject(errRes)
      })
    },
    fetch: ({ commit, state, dispatch }) => {
      return ghost.fetchUser().then(res => {
        if (res.length > 0) {
          let user = res[0]
          commit('init', {
            ...user,
            logined: true
          })
          return Promise.resolve(res[0])
        } else {
          return Promise.reject(res[0])
        }
      }, error => {
        commit('init', {
          logined: false
        })
        return Promise.reject(error)
      })
    },
    update: ({ commit }, { userId, updateData }) => {
      return ghost.updateUser(userId, updateData).then(data => {
        commit('init', data)
      })
    },
    reset: ({ commit, dispatch }) => {
      commit('token/clear', 'eagle', { root: true })
      commit('token/clear', 'eider', { root: true })
      commit('token/clear', 'donald', { root: true })
      commit('token/clear', 'fox', { root: true })
      commit('token/clear', 'ghost', { root: true })
      commit('reset')
    }
  }
}
