import Vue from 'vue'
import VueCompositionApi from '@vue/composition-api'
import moment from 'vue-moment'
import Vue2Filters from 'vue2-filters'

import './element'
Vue.use(VueCompositionApi)
Vue.use(moment)
Vue.use(Vue2Filters)
