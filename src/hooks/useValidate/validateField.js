import { isNullOrUndefined, isString, getValueAndMessage, isRegex } from './util'
import { MSG_REQUIRED, MSG_MAX, MSG_MIN, MSG_MIN_LENGTH, MSG_MAX_LENGTH } from './errorMessages'

export default async function validateField (ref, {
  required,
  maxLength,
  minLength,
  min,
  max,
  pattern
}) {
  const error = await new Promise(resolve => {
    const error = {}
    const { value, type, name } = ref

    if (required && isNullOrUndefined(value)) {
      error[name] = {
        type: 'required',
        message: isString(required) ? required : MSG_REQUIRED,
        ref
      }
      resolve(error)
      return
    }

    if (!isNullOrUndefined(min) || !isNullOrUndefined(max)) {
      let exceedMax
      let exceedMin
      const { value: maxValue, message: maxMessage } = getValueAndMessage(max, `${MSG_MAX}${max}`)
      const { value: minValue, message: mixMessage } = getValueAndMessage(min, `${MSG_MIN}${min}`)

      if (type === 'number') {
        const valueNumber = parseFloat(value)
        if (!isNullOrUndefined(maxValue)) {
          exceedMax = valueNumber > maxValue
        }
        if (!isNullOrUndefined(minValue)) {
          exceedMin = valueNumber < minValue
        }
      } else {
        if (isString(maxValue)) {
          exceedMax = new Date(value) > new Date(maxValue)
        }
        if (isString(minValue)) {
          exceedMin = new Date(value) < new Date(minValue)
        }
      }

      if (exceedMax || exceedMin) {
        const message = exceedMax ? maxMessage : mixMessage
        error[name] = {
          type: exceedMax ? 'max' : 'min',
          message,
          ref
        }
        resolve(error)
      }
      return
    }

    if ((maxLength || minLength) && isString(value)) {
      const {
        value: maxLengthValue,
        message: maxLengthMessage
      } = getValueAndMessage(maxLength, MSG_MAX_LENGTH.replace('$', maxLength))
      const {
        value: minLengthValue,
        message: minLengthMessage
      } = getValueAndMessage(minLength, MSG_MIN_LENGTH.replace('$', minLength))

      const inputLength = value.toString().length
      const exceedMax = maxLength && inputLength > maxLengthValue
      const exceedMin = minLength && inputLength < minLengthValue

      if (exceedMax || exceedMin) {
        const message = exceedMax ? maxLengthMessage : minLengthMessage
        error[name] = {
          type: exceedMax ? 'maxLength' : 'minLength',
          message,
          ref
        }
        resolve(error)
        return
      }
    }

    if (pattern) {
      const { value: patternValue, message: patternMessage } = getValueAndMessage(pattern)
      if (isRegex(patternValue) && !patternValue.test(value)) {
        error[name] = {
          type: 'pattern',
          message: patternMessage,
          ref
        }
        resolve(error)
      }
    }
  })
  return error
}
