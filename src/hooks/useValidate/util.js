export const isNullOrUndefined = value => {
  return value === null || value === undefined || value === ''
}

export const getValueAndMessage = (validationData, defaultErrorMsg = '') => {
  return {
    value:
      isObject(validationData) && !isRegex(validationData)
        ? validationData.value
        : validationData,
    message:
      isObject(validationData) && !isRegex(validationData)
        ? validationData.message
        : defaultErrorMsg
  }
}

export const isString = value => typeof value === 'string'

export const isArray = value => Array.isArray(value)

export const isObject = value => Object.prototype.toString.call(value) === '[object Object]'

export const isRegex = value => value instanceof RegExp
